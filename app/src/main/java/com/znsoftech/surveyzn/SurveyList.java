package com.znsoftech.surveyzn;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.znsoftech.surveyzn.data_structure.SurveyDetailInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SurveyList extends AppCompatActivity {

   SQLiteDatabase database;
   private DbHelper dbHelper;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_survey_list);

	  dbHelper = new DbHelper(this);
	  database = dbHelper.getWritableDatabase();
	  new QueryData().execute();
   }

   public void export(View view) throws JSONException {
	  ExportData exportData=new ExportData();
	  JSONObject js=new JSONObject();


	  js.put("AnswersArray",exportData.jsonArrayReturnTableData(database, Config.TABLE_ANSWERS,
			  null));

	  js.put("ConsumerProfile", exportData.jsonArrayReturnTableData(database, Config
			  .TABLE_CONSUMER_PROFILE, null));



	  Toast.makeText(this,js.toString(),Toast.LENGTH_LONG).show();
	  Log.e("data",js.toString());
   }



   class QueryData extends AsyncTask<Void, Void, ArrayList<SurveyDetailInfo>> {

	  @Override
	  protected void onPreExecute() {
		 super.onPreExecute();
	  }

	  @Override
	  protected ArrayList<SurveyDetailInfo> doInBackground(Void... voids) {
		 ArrayList<SurveyDetailInfo> data;
		 data = dbHelper.querySurveysAll(database);
		 return data;
	  }

	  @Override
	  protected void onPostExecute(final ArrayList<SurveyDetailInfo> surveyDetailInfos) {
//		 super.onPostExecute(surveyDetailInfos);
		 ListView listView = (ListView) findViewById(R.id.list_survey_list);
		 listView.setClickable(true);

		 listView.setAdapter(new AdapterListCustomSurvey(surveyDetailInfos, getApplicationContext()));
		 listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			   Intent questions = new Intent(SurveyList.this, ConsumerProfile.class);

			   questions.putExtra(Config.db.SURVEY_ID, surveyDetailInfos.get(i).getSurveyId());
			   startActivity(questions);
			}
		 });
	  }
   }

   @Override
   public void onBackPressed() {
	  super.onBackPressed();
   }
}
