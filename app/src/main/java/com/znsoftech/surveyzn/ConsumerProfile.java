package com.znsoftech.surveyzn;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.znsoftech.surveyzn.custom_ui.DialogCustom;
import com.znsoftech.surveyzn.data_structure.Question;
import com.znsoftech.surveyzn.data_structure.QuestionOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ConsumerProfile extends AppCompatActivity implements GoogleApiClient
		.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

   static final int REQUEST_TAKE_PHOTO = 1;
   private static final int REQUEST_CAMERA = 1;
   private static int CURRENT_QUESTION = 0;
   DbHelper dbHelper;
   SQLiteDatabase database;
   EditText editTextFirstName,
		   editTextmiddleName,
		   editTextlastName,
		   editTextaddress_line1,
		   editTextaddress_line2,
		   editTextaddress_city,
		   editTextaddress_state,
		   editTextaddress_country,
		   editTextzipcode,
		   editTextemail,
		   editTextphone,
		   editTextmobile;
   Button editTextdob;
   RadioGroup radioGroupGender, radioGroupStatus;
   RadioButton radioGender, radioMaritalStatus;
   ImageView imageViewProfile;
   //Questionsdata
   TextView questionText;
   EditText optionText;
   LinearLayout linearLayoutOption;
   RadioGroup optionsRadioBox;
   RadioGroup optionsCheckBox;
   private String dateDOB;
   private long _ID;
   private String SurveyID = null;
   private String imageFileName;
   //Profile Consumer data
   private ArrayList<Question> questionArrayList;
   private RadioGroup radioGroupAnswers;
   private ArrayList<CheckBox> checkBoxArraylist;
   private EditText editTextAnswers;
   private GoogleApiClient googleApiClient;
   private double lastLatitude;
   private double lastLongitude;
   private Question question;
   private ArrayList<QuestionOptions> arrayListQuestionOptions;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.profile_consumer);


	  googleApiClient = new GoogleApiClient.Builder(this)
			  .addConnectionCallbacks(this)
			  .addOnConnectionFailedListener(this)
			  .addApi(LocationServices.API)
			  .build();


	  editTextFirstName = (EditText) findViewById(R.id.efirstname);

	  editTextmiddleName = (EditText) findViewById(R.id.emiddlename);
	  editTextlastName = (EditText) findViewById(R.id.elastname);
	  editTextdob = (Button) findViewById(R.id.edob);
	  editTextaddress_line1 = (EditText) findViewById(R.id.eaddress_line1);
	  editTextaddress_line2 = (EditText) findViewById(R.id.eaddress_line2);
	  editTextaddress_city = (EditText) findViewById(R.id.eaddress_city);
	  editTextaddress_country = (EditText) findViewById(R.id.eaddress_country);
	  editTextaddress_state = (EditText) findViewById(R.id.eaddress_state);
	  editTextzipcode = (EditText) findViewById(R.id.eaddress_zipcode);
	  editTextemail = (EditText) findViewById(R.id.eemail);
	  editTextphone = (EditText) findViewById(R.id.ephone);
	  editTextmobile = (EditText) findViewById(R.id.emobile);
	  radioGroupGender = (RadioGroup) findViewById(R.id.radiogroup_gender);
	  radioGroupStatus = (RadioGroup) findViewById(R.id.radiogroup_maritalstatus);

	  optionText = new EditText(this);
	  linearLayoutOption = (LinearLayout) findViewById(R.id.lin_answers);


	  Intent intent = getIntent();
	  SurveyID = intent.getStringExtra(Config.db.SURVEY_ID);

	  dbHelper = new DbHelper(this);
	  database = dbHelper.getWritableDatabase();

	  questionArrayList = dbHelper.getQuestions(database, SurveyID);

	  imageViewProfile = (ImageView) findViewById(R.id.i_profile_consumer);


   }

   public void getImageConsumer(View view) {
	  dispatchTakePictureIntent();
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
		 Bundle bundle = data.getExtras();
		 Bitmap imageCamera = (Bitmap) bundle.get("data");
		 imageViewProfile.setImageBitmap(imageCamera);

		 String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
		 imageFileName = "Profile_" + timeStamp + ".jpeg";
		 String storageDir = Environment.getExternalStorageDirectory().getAbsolutePath()
				 + "/SurveyZN";

		 File storageDirect = new File(storageDir);
		 if (!storageDirect.exists()) {
			storageDirect.mkdir();
		 }
		 File storageDirFile = new File(storageDir + File.separator + SurveyID);
		 if (!storageDirFile.exists()) {
			storageDirFile.mkdir();
		 }

		 try {
			FileOutputStream fileOutputStream = new FileOutputStream(storageDir + File
					.separator + imageFileName);
			imageCamera.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
			fileOutputStream.close();
		 } catch (FileNotFoundException e) {
			e.printStackTrace();
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	  }
   }

   private void dispatchTakePictureIntent() {
	  Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	  startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
   }

   public void saveConsumerProfile(View view) {
	  String firstname, lastname, middlename, dob, gender=Config.UNMARRIED, maritalStatus=Config
			  .NOT_SPECIFIED,
	  addressline1,
			  addressline2,
			  address_city, address_state, address_country, address_zipcode, email, phone, latitude,
			  mobile,
			  longitude, photo, createdOn, createdBy, modifiedOn;

	  firstname = editTextFirstName.getText().toString();
	  middlename = editTextmiddleName.getText().toString();
	  lastname = editTextlastName.getText().toString();
	  addressline1 = editTextaddress_line1.getText().toString();
	  addressline2 = editTextaddress_line2.getText().toString();
	  address_city = editTextaddress_city.getText().toString();
	  address_state = editTextaddress_state.getText().toString();
	  address_country = editTextaddress_country.getText().toString();
	  address_zipcode = editTextzipcode.getText().toString();
	  email = editTextemail.getText().toString();
	  phone = editTextphone.getText().toString();
	  mobile = editTextmobile.getText().toString();
	  dob = editTextdob.getText().toString();
	  radioGender = (RadioButton) findViewById(radioGroupGender.getCheckedRadioButtonId());
	  if(radioGender!=null){
		 gender = radioGender.getText().toString();
	  }else {
		 // TODO: 02/12/15  
	  }

	  radioMaritalStatus = (RadioButton) findViewById(radioGroupStatus.getCheckedRadioButtonId());
	  		photo = imageFileName;
	  if (radioMaritalStatus!=null){
		 maritalStatus = radioMaritalStatus.getText().toString();
	  }else {
		 // TODO: 02/12/15
	  }


	  latitude = Config.LATITUDE_CURRENTUSER;
	  longitude = Config.LONGITUDE_CURRENTUSER;

	  String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

	  createdOn = currentDateTimeString;
	  createdBy = Config.SURVEYOR_ID_LOGGED_IN;

	  modifiedOn = currentDateTimeString;

	  int error=0;
	  String errorString="";

	  if (firstname.equals("")){
		 error=1;
		 errorString=errorString+"Firstname must not be empty\n";
	  }
	  if (middlename.equals("")){
//	  error=1;
	  }
	  if (lastname.equals("")){
//	  error=1;
	  }
	  if (addressline1.equals("")){
	  	error=1;
		 errorString=errorString+"AddressLine1 must not be empty\n";
	  }
	  if (addressline2.equals("")){
//	  error=1;
	  }
	  if (address_city.equals("")){
	  error=1;
		 errorString=errorString+"address must not be empty\n";
	  }
	  if (address_country.equals("")){
	  error=1;
		 errorString=errorString+"country must not be empty\n";
	  }
	  if (address_state.equals("")){

//	  error=1;
	  }
	  if (address_zipcode.equals("")){
		 errorString=errorString+"pincode must not be empty\n";
	  error=1;
	  }
	  if (mobile.equals("")){
		 errorString=errorString+"mobile must not be empty\n";
	  error=1;
	  }
	  if (phone.equals("")){
//	  error=1;
	  }
	  if (dob.equals("")){
		 errorString=errorString+"dob must not be empty\n";
	  error=1;
	  }
	  if (email.equals("")){
		 errorString=errorString+"email must not be empty\n";
	  error=1;
	  }
	  if (maritalStatus.equals("")){
		 errorString=errorString+"marital status must not be empty\n";
	  error=1;
	  }

	  if (error==1){
		 DialogCustom dialogCustom=new DialogCustom();
		 dialogCustom.init("Error", "Error! " + errorString, null, "OK", null, new
				 DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					   // TODO: 04/12/15

					}
		 });
		 dialogCustom.show(getSupportFragmentManager(),"dialog");

	  }
	  else {
		 _ID = dbHelper.insertConsumerProfiles(database, SurveyID, Config.SURVEYOR_ID_LOGGED_IN,
				 firstname,
				 middlename, lastname, addressline1 + " ," + addressline2, address_city, address_state,
				 address_country, address_zipcode, mobile, phone, gender, photo, dob, email, createdOn,
				 createdBy, modifiedOn, latitude, longitude, maritalStatus);
		 if (_ID != -1) {
			setContentView(R.layout.question_answer);
			getCurrentQuestion();

		 } else {
			Toast.makeText(getApplicationContext(), "Error saving profile", Toast.LENGTH_LONG).show();
		 }
	  }
   }

   public void getD(View v) {
	  final android.app.DatePickerDialog.OnDateSetListener dateSetListener = new android.app
			  .DatePickerDialog
			  .OnDateSetListener() {
		 @Override
		 public void onDateSet(DatePicker datePicker, int year, int month, int day) {
			dateDOB = String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(day);
			editTextdob.setText(dateDOB);
		 }
	  };
	  DialogDatePick datePickerDialog = new DialogDatePick(this, dateSetListener, 1990, 1, 1);
	  datePickerDialog.show();
   }


   public int getCurrentQuestion() {
//	  if (!questionArrayList.isEmpty()) {


		 if (questionArrayList.size() > CURRENT_QUESTION||questionArrayList.isEmpty()) {


			question = questionArrayList.get(CURRENT_QUESTION);
			arrayListQuestionOptions = dbHelper.getQuestionsOptions(database,
					question.getQuestionId());


			if (arrayListQuestionOptions.size() > 0) {
			   insertViewInLinear(this, arrayListQuestionOptions);
			   questionText = (TextView) findViewById(R.id.tquestion);
			   questionText.setText(question.getQuestion());
			} else return Config.NO_QUESTION_OPTION;
		 } else return Config.NO_MORE_QUESTION;
//	  } else Toast.makeText(this, "No Question", Toast.LENGTH_LONG).show();
	  return Config.SUCCESS;
   }

   public void insertViewInLinear(Context context, ArrayList<QuestionOptions>
		   arrayList) {
//	  setContentView(R.layout.question_answer);
	  linearLayoutOption = (LinearLayout) findViewById(R.id.lin_answers);

	  linearLayoutOption.removeAllViewsInLayout();

	  editTextAnswers = null;
	  if (arrayList.size() > 0) {

		 if (arrayList.get(0).getQuestionOptionType().equals(Config.OPTION_TYPE_RADIO)) {
			radioGroupAnswers = new RadioGroup(context);
			for (int i = 0; i < arrayList.size(); i++) {
			   QuestionOptions questionOptions = arrayList.get(i);
			   RadioButton radioButton = new RadioButton(context);
			   radioButton.setTextColor(Color.parseColor("#ffffff"));
			   radioButton.setText(questionOptions.getQuestionOptionText());
			   radioGroupAnswers.addView(radioButton);
			}
			linearLayoutOption.addView(radioGroupAnswers);

			checkBoxArraylist = null;
			editTextAnswers = null;

		 } else if (arrayList.get(0).getQuestionOptionType().equals(Config.OPTION_TYPE_CHECKBOX)) {
			checkBoxArraylist = new ArrayList<>();
			for (int i = 0; i < arrayList.size(); i++) {
			   QuestionOptions questionOptions = arrayList.get(i);

			   CheckBox checkBox = new CheckBox(context);
			   checkBox.setText(questionOptions.getQuestionOptionText());
			   checkBox.setTextColor(Color.parseColor("#ffffff"));
			   linearLayoutOption.addView(checkBox);
			   checkBoxArraylist.add(checkBox);

			   radioGroupAnswers = null;
			   editTextAnswers = null;
			}
		 } else if (arrayList.get(0).getQuestionOptionType().equals(Config.OPTION_TYPE_EDITTEXT)) {
			editTextAnswers = new EditText(context);
			editTextAnswers.setHint("Add text here");
			editTextAnswers.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams
					.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			editTextAnswers.setHintTextColor(getResources().getColor(R.color.white));
			editTextAnswers.setTextColor(getResources().getColor(R.color.white));
			linearLayoutOption.addView(editTextAnswers);

			checkBoxArraylist = null;
			radioGroupAnswers = null;
		 }
	  } else Toast.makeText(context, "No Option availabel", Toast.LENGTH_LONG).show();

   }

   private void processData() {
	  String createdOn, createdBy, modifiedOn;

	  String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

	  createdOn = currentDateTimeString;
	  createdBy = Config.SURVEYOR_ID_LOGGED_IN;

	  modifiedOn = currentDateTimeString;

	  if (editTextAnswers != null) {
		 dbHelper.insertAnswers(database, SurveyID, questionArrayList.get(CURRENT_QUESTION).getQuestionId
						 (), String.valueOf(_ID), editTextAnswers.getText().toString(), Config
						 .OPTION_TYPE_EDITTEXT, Config.LATITUDE_CURRENTUSER, Config.LONGITUDE_CURRENTUSER,
				 createdOn, createdBy,
				 modifiedOn);


	  } else if (radioGroupAnswers != null) {
		 if (radioGroupAnswers.getCheckedRadioButtonId() != -1) {
			RadioButton radioButton = (RadioButton) findViewById(radioGroupAnswers.getCheckedRadioButtonId());
			int index = radioGroupAnswers.indexOfChild(radioButton);
			String optionId = arrayListQuestionOptions.get(index)
					.getQuestionOptionId();

			dbHelper.insertAnswers(database, SurveyID, questionArrayList.get(CURRENT_QUESTION).getQuestionId
							(), String.valueOf(_ID), optionId, Config
							.OPTION_TYPE_RADIO, Config.LATITUDE_CURRENTUSER, Config.LONGITUDE_CURRENTUSER,
					createdOn, createdBy,
					modifiedOn);
		 }
	  } else if (checkBoxArraylist != null) {

		 String answerIDS = "";

		 for (int j = 0; j < checkBoxArraylist.size(); j++) {
			if (checkBoxArraylist.get(j).isChecked()) {
			   answerIDS = answerIDS + arrayListQuestionOptions.get(j)
					   .getQuestionOptionId()+"$";
			}
		 }
		 if (answerIDS.endsWith("$")) {
			answerIDS = answerIDS.substring(0, answerIDS.length() - 1);
		 }


		 dbHelper.insertAnswers(database, SurveyID, questionArrayList.get(CURRENT_QUESTION).getQuestionId
						 (), String.valueOf(_ID), answerIDS.toString(), Config
						 .OPTION_TYPE_CHECKBOX, Config.LATITUDE_CURRENTUSER, Config.LONGITUDE_CURRENTUSER,
				 createdOn, createdBy,
				 modifiedOn);
	  }
   }

   public void clickNext(View view) {
		 processData();
		 ++CURRENT_QUESTION;
	  int status=getCurrentQuestion();
		 if (status==Config.NO_MORE_QUESTION){
			Toast.makeText(this,"No More Questions Left",Toast.LENGTH_LONG).show();
			setContentView(R.layout.finishquestionaire);
		 }else if (status==Config.NO_QUESTION_OPTION){
			Toast.makeText(this,"This question has no optioin",Toast.LENGTH_LONG).show();
		 }else if (status==Config.SUCCESS){
			//TODO SOMETHING
		 }

   }


   @Override
   public void onConnected(Bundle bundle) {
	  Location lastlocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
	  if (lastlocation != null) {
		 lastLatitude = lastlocation.getLatitude();
		 lastLongitude = lastlocation.getLongitude();
		 //Request location updates after 10seconds minimum and 5seconds maximus
		 LocationRequest mLocationRequest = new LocationRequest();
		 mLocationRequest.setInterval(10000);
		 mLocationRequest.setFastestInterval(5000);
		 mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		 LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
				 mLocationRequest, this);
	  }
   }

   @Override
   public void onConnectionSuspended(int i) {

   }

   @Override
   public void onLocationChanged(Location location) {

	  lastLatitude = location.getLatitude();
	  lastLongitude = location.getLongitude();
	  Toast.makeText(this, String.valueOf(lastLatitude) + "-----" + String.valueOf(lastLongitude), Toast
			  .LENGTH_SHORT).show();
	  Config.LATITUDE_CURRENTUSER = String.valueOf(lastLatitude);
	  Config.LONGITUDE_CURRENTUSER = String.valueOf(lastLongitude);
   }

   @Override
   public void onConnectionFailed(ConnectionResult connectionResult) {

   }

   public void finishsurvey(View view) {
	  CURRENT_QUESTION=0;
	  Intent intent=new Intent(this,SurveyList.class);
	  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
	  startActivity(intent);

   }

   public void nextprofile(View view) {
	  CURRENT_QUESTION=0;
	recreate();
   }
}