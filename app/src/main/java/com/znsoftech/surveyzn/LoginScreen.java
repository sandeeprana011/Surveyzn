package com.znsoftech.surveyzn;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.znsoftech.surveyzn.custom_ui.DialogCustom;
import com.znsoftech.surveyzn.network.DownloadContentAndDecode;
import com.znsoftech.surveyzn.network.ReportToZn;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class LoginScreen extends AppCompatActivity implements GoogleApiClient
		.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

   private SharedPreferences preferences;
   private GoogleApiClient googleApiClient;
   private double lastLatitude,lastLongitude;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.loginscreen);

	  preferences = getSharedPreferences(Config.SETTINGS, MODE_PRIVATE);

	  googleApiClient = new GoogleApiClient.Builder(this)
			  .addConnectionCallbacks(this)
			  .addOnConnectionFailedListener(this)
			  .addApi(LocationServices.API)
			  .build();
	  if (!isGPSEnabled(this)) {
		 exitIfGPSisOff();
	  }
	  //switch to only gps
	  onlyGPS();

	  if (preferences.getBoolean(Config.LOGGED_IN,false)){
		 Intent intent = new Intent(LoginScreen.this, SurveyList.class);
		 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
		 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		 startActivity(intent);
	  }


   }
   public void loginButton(View view) {
	  EditText editUser= (EditText) findViewById(R.id.eusername_surveyor_login);
	  EditText editPass= (EditText) findViewById(R.id.epassword_surveyor_password);

	  String url=Config.URL_PROFILE;

	  String username=editUser.getText().toString();
	  String password=editPass.getText().toString();

	  if (username!=null&&!(username.length()<=0)&&!(password.length()<=0)){
		 InitializeApplication initializeApplication=new InitializeApplication();
		 initializeApplication.execute(url, username, password);
	  }

   }


   @Override
   protected void onStart() {
	  super.onStart();
	  googleApiClient.connect();
   }

   @Override
   protected void onStop() {
	  super.onStop();
	  googleApiClient.disconnect();
   }



   public void exitIfGPSisOff() {
	  AlertDialog.Builder builder = new AlertDialog.Builder(this);
	  builder.setMessage("App wouldn't run without GPS. Turn GPS on and restart the Application?")
			  .setCancelable(false)
			  .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int id) {
					System.exit(0);
				 }
			  });
	  AlertDialog alert = builder.create();
	  alert.show();
   }

   public boolean isGPSEnabled(Context context) {
	  LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	  return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
   }

   /**
	* Only for gps not for network base locations
	*/
   public void onlyGPS() {
	  android.location.LocationListener locationListener = new android.location.LocationListener() {
		 @Override
		 public void onLocationChanged(Location location) {
			Log.e("location", String.valueOf(location.getAccuracy()) + "    \n" + String.valueOf
					(location.getLatitude()) + "    \n" + String.valueOf(location.getLongitude()));
		 }

		 @Override
		 public void onStatusChanged(String provider, int status, Bundle extras) {

		 }

		 @Override
		 public void onProviderEnabled(String provider) {

		 }

		 @Override
		 public void onProviderDisabled(String provider) {

		 }
	  };
	  LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	  if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
			  PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission
			  .ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

		 Log.e("allowed", "permitted");
		 manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		 return;
	  }
   }
   @Override
   public void onConnected(Bundle bundle) {



	  Location lastlocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
	  if (lastlocation != null) {
		 lastLatitude = lastlocation.getLatitude();
		 lastLongitude = lastlocation.getLongitude();
		 //Request location updates after 10seconds minimum and 5seconds maximus
		 LocationRequest mLocationRequest = new LocationRequest();
		 mLocationRequest.setInterval(1000);
		 mLocationRequest.setFastestInterval(500);
		 mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		 LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
				 mLocationRequest, this);
	  }
   }

   @Override
   public void onConnectionSuspended(int i) {

   }

   @Override
   public void onConnectionFailed(ConnectionResult connectionResult) {

   }

   @Override
   public void onLocationChanged(Location location) {
	  lastLatitude = location.getLatitude();
	  lastLongitude = location.getLongitude();
	  Toast.makeText(this, String.valueOf(lastLatitude) + "-----" + String.valueOf(lastLongitude), Toast
			  .LENGTH_SHORT).show();
	  Config.LATITUDE_CURRENTUSER = String.valueOf(lastLatitude);
	  Config.LONGITUDE_CURRENTUSER = String.valueOf(lastLongitude);
   }

   @Override
   protected void onDestroy() {
	  super.onDestroy();
	  googleApiClient.disconnect();
	  googleApiClient=null;
   }

   @Override
   public void onBackPressed() {
	  super.onBackPressed();
	  System.exit(0);
   }

   class InitializeApplication extends AsyncTask<String, Integer, Integer> {

	  String progressStatus="Initializing.",progressStatussub;
	  int progInt,progIntSub;
	  TextView textfinal,textsub;



	  String psurveyorId, psurveyorcode, pfirstname, pmiddlename, plastname, paddress, pcity, pstate,
			  pcountry,
			  pzipcode, pmobile, pphone, palternateNumber, pgender, pdob, plogin, ppassword, pemail,
			  pcreatedon,
			  pcreatedby, pmodifiedon;

	  String ssurveyId, ssurveyname, ssurveydesc, ssurveystartdate, ssurveyenddate, screatedon,
			  screatedby, smodifiedon, ssurveytypeid, ssurveytypename, ssurveytypedesc;

	  String qsurveyid, qquestionid, qquestion, qcreatedon, qcreatedby, qmodifiedon;

	  String oquestionoptionid, oquestionoption, oquestionoptiontypeid, oquestionoptiontype,
			  oquestionid,
			  ocreatedon, ocreatedby, omodifiedon;

	  DbHelper dbHelper;
	  ProgressBar progressBarFinal,progressBarSub;
//	  DownloadContentAndDecode downloadContentAndDecode;

	  private boolean isDownloadAndInsertOk = false;
	  private String messageError="";


	  @Override
	  protected void onPreExecute() {
		 super.onPreExecute();
		 //progressStatus="Initializing..";

//		 data = "{" +
//				 "  \"status\": 200," +
//				 "  \"status_message\": \"Record Fetched\"," +
//				 "  \"surveyor_data\": {" +
//				 "    \"profile\": " +
//				 "      {" +
//				 "        \"SurveyorId\": \"1\"," +
//				 "        \"SurveyorCode\": \"12345\"," +
//				 "        \"FirstName\": \"Mohsin\"," +
//				 "        \"MiddleName\": \"Hasan\"," +
//				 "        \"LastName\": \"Ali\"," +
//				 "        \"Address\": \"Batla House\"," +
//				 "        \"City\": \"New Delhi\"," +
//				 "        \"State\": \"Delhi\"," +
//				 "        \"Country\": \"India\"," +
//				 "        \"ZipCode\": \"110025\"," +
//				 "        \"Mobile\": \"456465\"," +
//				 "        \"Phone\": \"214748\"," +
//				 "        \"AlternatePhone\": \"46546\"," +
//				 "        \"Gender\": \"MALE\"," +
//				 "        \"DOB\": \"2015-08-12\"," +
//				 "        \"Login\": \"user\"," +
//				 "        \"Password\": \"12345\"," +
//				 "        \"Email\": \"mohsin@znssoftech.com\"," +
//				 "        \"CreatedOn\": \"2015-08-11\"," +
//				 "        \"CreatedBy\": \"Mohsin\"," +
//				 "        \"ModifiedOn\": \"2015-08-11\"" +
//				 "      }" +
//				 "    ," +
//				 "    \"surveys\": [" +
//				 "      {" +
//				 "        \"SurveyId\": \"1\"," +
//				 "        \"SurveyName\": \"FirstSurvey\"," +
//				 "        \"SurveyDesc\": \"Survey for checking\"," +
//				 "        \"SurveyStartDate\": \"2015-08-12\"," +
//				 "        \"SurveyEndDate\": \"2015-08-31\"," +
//				 "        \"SurveyTypeId\": \"1\"," +
//				 "        \"SurveyType\":\"\"" +
//				 "      }" +
//				 "    ]," +
//				 "    \"questions\": [" +
//				 "      {" +
//				 "        \"SurveyId\": \"1\"," +
//				 "        \"QuestionID\": \"1\"," +
//				 "        \"Question\": \"How satisfied are you with our products?\"," +
//				 "        \"CreatedOn\": \"2015-08-19\"," +
//				 "        \"CreatedBy\": \"Mohsin\"," +
//				 "        \"ModifiedOn\": \"2015-08-19\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"SurveyId\": \"1\"," +
//				 "        \"QuestionID\": \"2\"," +
//				 "        \"Question\": \"What would you like to eat?\"," +
//				 "        \"CreatedOn\": \"2015-08-19\"," +
//				 "        \"CreatedBy\": \"Mohsin\"," +
//				 "        \"ModifiedOn\": \"2015-08-19\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"SurveyId\": \"1\"," +
//				 "        \"QuestionID\": \"3\"," +
//				 "        \"Question\": \"Do you have any feedback for our products?\"," +
//				 "        \"CreatedOn\": \"2015-08-19\"," +
//				 "        \"CreatedBy\": \"Mohsin\"," +
//				 "        \"ModifiedOn\": \"2015-08-19\"" +
//				 "      }" +
//				 "    ]," +
//				 "    \"questionsoptions\": [" +
//				 "      {" +
//				 "        \"QuestionID\": \"1\"," +
//				 "        \"QuestionOption\": \"Very Satisfied\"," +
//				 "        \"QuestionOptionId\": \"1\"," +
//				 "        \"QuestionOptionTypeId\": \"3\"," +
//				 "        \"QuestionOptionType\": \"RadioButton\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"1\"," +
//				 "        \"QuestionOption\": \"Satisfied\"," +
//				 "        \"QuestionOptionId\": \"2\"," +
//				 "        \"QuestionOptionTypeId\": \"3\"," +
//				 "        \"QuestionOptionType\": \"RadioButton\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"1\"," +
//				 "        \"QuestionOption\": \"Neutral\"," +
//				 "        \"QuestionOptionId\": \"3\"," +
//				 "        \"QuestionOptionTypeId\": \"3\"," +
//				 "        \"QuestionOptionType\": \"RadioButton\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"1\"," +
//				 "        \"QuestionOption\": \"Not Satisfied\"," +
//				 "        \"QuestionOptionId\": \"4\"," +
//				 "        \"QuestionOptionTypeId\": \"3\"," +
//				 "        \"QuestionOptionType\": \"RadioButton\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"2\"," +
//				 "        \"QuestionOption\": \"Burger\"," +
//				 "        \"QuestionOptionId\": \"5\"," +
//				 "        \"QuestionOptionTypeId\": \"2\"," +
//				 "        \"QuestionOptionType\": \"CheckBox\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"2\"," +
//				 "        \"QuestionOption\": \"Pizza\"," +
//				 "        \"QuestionOptionId\": \"6\"," +
//				 "        \"QuestionOptionTypeId\": \"2\"," +
//				 "        \"QuestionOptionType\": \"CheckBox\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"2\"," +
//				 "        \"QuestionOption\": \"Ice Cream\"," +
//				 "        \"QuestionOptionId\": \"7\"," +
//				 "        \"QuestionOptionTypeId\": \"2\"," +
//				 "        \"QuestionOptionType\": \"CheckBox\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"2\"," +
//				 "        \"QuestionOption\": \"Coffee\"," +
//				 "        \"QuestionOptionId\": \"8\"," +
//				 "        \"QuestionOptionTypeId\": \"2\"," +
//				 "        \"QuestionOptionType\": \"CheckBox\"" +
//				 "      }," +
//				 "      {" +
//				 "        \"QuestionID\": \"3\"," +
//				 "        \"QuestionOption\": \"\"," +
//				 "        \"QuestionOptionId\": \"9\"," +
//				 "        \"QuestionOptionTypeId\": \"1\"," +
//				 "        \"QuestionOptionType\": \"EditText\"" +
//				 "      }" +
//				 "    ]" +
//				 "  }" +
//				 "}";
//		 setContentView(R.layout.initialization);
		 showLayoutInitialization();
//		 crete incancelable window

		 progressBarFinal= (ProgressBar) findViewById(R.id.finalprogress);
		 progressBarSub= (ProgressBar) findViewById(R.id.subprogress);
		 textfinal= (TextView) findViewById(R.id.tfinalprogress);
		 textsub= (TextView) findViewById(R.id.tsubfinalprogress);
		 dbHelper = new DbHelper(getApplicationContext());
		 //progressStatus="Initializing...";

//		 downloadContentAndDecode=new DownloadContentAndDecode();
		 //progressStatus="Initializing....";
	  }

	  @Override
	  protected Integer doInBackground(String... strings) {
		 //progressStatus="Creating Database....";
		 //publishProgress(progress,1);
		 updateProgress("Downloading Data....",5,"Authorising you",30);
		 SQLiteDatabase database = dbHelper.getWritableDatabase();
		 Log.d("Debug", database.getPath() + "------" + database.getPageSize());
//		 Log.d("Debug","");
		 //publishProgress(progress+interval,1);
		 //progressStatus="Database Created";

		 String urlRemote = strings[0];
		 String username = strings[1];
		 String password = strings[2];

//		 InputStream is = null;
		 try {


			Log.d("Debug", "downloading data");
			updateProgress("Downloading Data....", 5, "Authorising you", 35);

			String content= DownloadContentAndDecode.downloadContentUsingPostMethod(urlRemote,
					Config.param.USERNAME + "=" + username + "&" + Config.param.PASSWORD + "=" + password);

			updateProgress("Downloading Data....",10,"Authorising you",60);



//			StringBuilder responseString = new StringBuilder(content);

			Log.d("Debug", "parsing started");


			JSONObject jsonObject = new JSONObject(content);
//			JSONObject jsonObject = new JSONObject(String.valueOf(data));
			if (jsonObject.has(Config.STATUS) && !jsonObject.isNull(Config.STATUS) && jsonObject
					.getString(Config.STATUS).equals("200")) {

//			   if (jsonObject.getString(Config.STATUS).equals("200")) {


			   if (jsonObject.has(Config.SURVEYOR_DATA) && !jsonObject.isNull(Config.SURVEYOR_DATA)) {
				  JSONObject jSurveyorData = jsonObject.getJSONObject(Config.SURVEYOR_DATA);

				  //progressStatus="Analyisng data for you..";
				  //publishProgress(progress,1);

				  updateProgress("Downloading Data....",10,"Authorised",100);

				  Log.d("Debug", "Parsing profile");
				  //finding surveyor profile and inserting into Config.TABLE_SURVEYOR_PROFILE
				  if (jSurveyorData.has(Config.TABLE_SURVEYOR_PROFILE) && !jSurveyorData.isNull
						  (Config.TABLE_SURVEYOR_PROFILE)) {
					 JSONObject jsProfile = jSurveyorData.getJSONObject(Config
							 .TABLE_SURVEYOR_PROFILE);
					 //progressStatus="Analyisng data for you...";
					 //publishProgress(progress,1);

					 updateProgress("Parsing Data....",15,"Parsing Profile",20);

					 if (jsProfile.has(Config.db.SURVEYOR_ID) && !jsProfile.isNull(Config.db.SURVEYOR_ID)) {
						psurveyorId = jsProfile.getString(Config.db.SURVEYOR_ID);
						Config.SURVEYOR_ID_LOGGED_IN = psurveyorId;
					 } else psurveyorId = "";
					 if (jsProfile.has(Config.db.SURVEYOR_CODE) && !jsProfile.isNull(Config.db.SURVEYOR_CODE))
						psurveyorcode = jsProfile.getString(Config.db.SURVEYOR_CODE);
					 else psurveyorcode = "";
					 updateProgress("Parsing Data....",15,"Validating Profile",40);

					 //progressStatus="Analyisng data for you....";
					 //publishProgress(progress,5);

					 if (jsProfile.has(Config.db.FIRSTNAME) && !jsProfile.isNull(Config.db.FIRSTNAME))
						pfirstname = jsProfile.getString(Config.db.FIRSTNAME);
					 else pfirstname = "";
					 if (jsProfile.has(Config.db.MIDDLE_NAME) && !jsProfile.isNull(Config.db.MIDDLE_NAME))
						pmiddlename = jsProfile.getString(Config.db.MIDDLE_NAME);
					 else pmiddlename = "";
					 if (jsProfile.has(Config.db.LASTNAME) && !jsProfile.isNull(Config.db.LASTNAME))
						plastname = jsProfile.getString(Config.db.LASTNAME);
					 else plastname = "";

					 updateProgress("Parsing Data....",15,"Saving Profile",60);

					 //progressStatus="Analyisng data for you.";
					 //publishProgress(progress,10);

					 if (jsProfile.has(Config.db.ADDRESS) && !jsProfile.isNull(Config.db.ADDRESS))
						paddress = jsProfile.getString(Config.db.ADDRESS);
					 else paddress = "";
					 if (jsProfile.has(Config.db.CITY) && !jsProfile.isNull(Config.db.CITY))
						pcity = jsProfile.getString(Config.db.CITY);
					 else pcity = "";
					 if (jsProfile.has(Config.db.STATE) && !jsProfile.isNull(Config.db.STATE))
						pstate = jsProfile.getString(Config.db.STATE);
					 else pstate = "";
					 if (jsProfile.has(Config.db.COUNTRY) && !jsProfile.isNull(Config.db.COUNTRY))
						pcountry = jsProfile.getString(Config.db.COUNTRY);
					 else pcountry = "";
					 //progressStatus="Analyisng data for you..";
					 //publishProgress(progress,15);

					 if (jsProfile.has(Config.db.ZIPCODE) && !jsProfile.isNull(Config.db.ZIPCODE))
						pzipcode = jsProfile.getString(Config.db.ZIPCODE);
					 else pzipcode = "";
					 if (jsProfile.has(Config.db.MOBILE) && !jsProfile.isNull(Config.db.MOBILE))
						pmobile = jsProfile.getString(Config.db.MOBILE);
					 else pmobile = "";
					 if (jsProfile.has(Config.db.PHONE) && !jsProfile.isNull(Config.db.PHONE))
						pphone = jsProfile.getString(Config.db.PHONE);
					 else pphone = "";
					 if (jsProfile.has(Config.db.ALTERNATE_PHONE) && !jsProfile.isNull(Config.db
							 .ALTERNATE_PHONE))
						palternateNumber = jsProfile.getString(Config.db.ALTERNATE_PHONE);
					 else palternateNumber = "";
					 if (jsProfile.has(Config.db.GENDER) && !jsProfile.isNull(Config.db.GENDER))
						pgender = jsProfile.getString(Config.db.GENDER);
					 else pgender = "";
					 if (jsProfile.has(Config.db.DOB) && !jsProfile.isNull(Config.db.DOB))
						pdob = jsProfile.getString(Config.db.DOB);
					 else pdob = "";
					 if (jsProfile.has(Config.db.LOGIN) && !jsProfile.isNull(Config.db.LOGIN))
						plogin = jsProfile.getString(Config.db.LOGIN);
					 else plogin = "";

					 //progressStatus="Analyisng data for you...";
					 //publishProgress(progress,20);

					 if (jsProfile.has(Config.db.PASSWORD) && !jsProfile.isNull(Config.db.PASSWORD))
						ppassword = jsProfile.getString(Config.db.PASSWORD);
					 else ppassword = "";
					 if (jsProfile.has(Config.db.EMAIL) && !jsProfile.isNull(Config.db.EMAIL))
						pemail = jsProfile.getString(Config.db.EMAIL);
					 else pemail = "";
					 if (jsProfile.has(Config.db.CREATED_ON) && !jsProfile.isNull(Config.db.CREATED_ON))
						pcreatedon = jsProfile.getString(Config.db.CREATED_ON);
					 else pcreatedon = "";
					 if (jsProfile.has(Config.db.CREATED_BY) && !jsProfile.isNull(Config.db.CREATED_BY))
						pcreatedby = jsProfile.getString(Config.db.CREATED_BY);
					 else pcreatedby = "";
					 if (jsProfile.has(Config.db.MODIFIED_ON) && !jsProfile.isNull(Config.db.MODIFIED_ON))
						pmodifiedon = jsProfile.getString(Config.db.MODIFIED_ON);
					 else pmodifiedon = "";

					 //progressStatus="Analyisng data for you....";
					 //publishProgress(progress,23);

					 //progressStatus="Saving your profile....";
					 //publishProgress(progress, 25);
					 Log.d("Debug", "Profile insertion");

					 dbHelper.insertSurveyorProfile(database, psurveyorId, psurveyorcode,
							 pfirstname,
							 pmiddlename, plastname, paddress, pcity, pstate, pcountry, pzipcode,
							 pmobile, pphone, palternateNumber, pgender, pdob, plogin, ppassword,
							 pemail, pcreatedon, pcreatedby, pmodifiedon, String.valueOf
									 (lastLatitude), String.valueOf(lastLongitude), "");

					 updateProgress("Parsing Data....", 15, "Parsing Profile Verfied", 100);


				  }

				  //finding and inserting into surveys

				  //progressStatus="Saving surveys....";
				  //publishProgress(progress,30);

//					 int subUpPre = 30;
				  if (jSurveyorData.has(Config.TABLE_SURVEYS) && !jSurveyorData.isNull(Config
						  .TABLE_SURVEYS) && jSurveyorData.getJSONArray(Config.TABLE_SURVEYS)
						  .length() > 0) {
					 JSONArray jsurveys = jSurveyorData.getJSONArray(Config.TABLE_SURVEYS);

					 updateProgress("Parsing Data....",25,"Analyzing Surveys",0);
					 int l=0;
					 int subUp = 100 / jsurveys.length();
					 for (int i = 0; i < jsurveys.length(); i++) {
						l=l+subUp;
						updateProgress("Parsing Data....",25,"Analyzing Surveys",l);
						//progressStatus="Saving surveys....";
						//publishProgress(progress,subUpPre+subUp);
						Log.d("Debug", "Survey Insertion");
						JSONObject js = jsurveys.getJSONObject(i);
						if (js.has(Config.db.SURVEY_ID) && !js.isNull(Config.db.SURVEY_ID))
						   ssurveyId = js.getString(Config.db.SURVEY_ID);
						else ssurveyId = "";
						if (js.has(Config.db.SURVEY_NAME) && !js.isNull(Config.db.SURVEY_NAME))
						   ssurveyname = js.getString(Config.db.SURVEY_NAME);
						else ssurveyname = "";
						if (js.has(Config.db.SURVEY_DESC) && !js.isNull(Config.db.SURVEY_DESC))
						   ssurveydesc = js.getString(Config.db.SURVEY_DESC);
						else ssurveydesc = "";
						if (js.has(Config.db.SURVEY_START_DATE) && !js.isNull(Config.db.SURVEY_START_DATE))
						   ssurveystartdate = js.getString(Config.db.SURVEY_START_DATE);
						else ssurveystartdate = "";
						if (js.has(Config.db.SURVEY_END_DATE) && !js.isNull(Config.db.SURVEY_END_DATE))
						   ssurveyenddate = js.getString(Config.db.SURVEY_END_DATE);
						else ssurveyenddate = "";
						if (js.has(Config.db.CREATED_ON) && !js.isNull(Config.db.CREATED_ON))
						   screatedon = js.getString(Config.db.CREATED_ON);
						else screatedon = "";
						if (js.has(Config.db.CREATED_BY) && !js.isNull(Config.db.CREATED_BY))
						   screatedby = js.getString(Config.db.CREATED_BY);
						else screatedby = "";
						if (js.has(Config.db.MODIFIED_ON) && !js.isNull(Config.db.MODIFIED_ON))
						   smodifiedon = js.getString(Config.db.MODIFIED_ON);
						else smodifiedon = "";
						if (js.has(Config.db.SURVEY_TYPE_ID) && !js.isNull(Config.db.SURVEY_TYPE_ID))
						   ssurveytypeid = js.getString(Config.db.SURVEY_TYPE_ID);
						else ssurveytypeid = "";
						if (js.has(Config.db.SURVEY_TYPE_NAME) && !js.isNull(Config.db.SURVEY_TYPE_NAME))
						   ssurveytypename = js.getString(Config.db.SURVEY_TYPE_NAME);
						else ssurveytypename = "";
						if (js.has(Config.db.SURVEY_TYPE_DESC) && !js.isNull(Config.db.SURVEY_TYPE_DESC))
						   ssurveytypedesc = js.getString(Config.db.SURVEY_TYPE_DESC);
						else ssurveytypedesc = "";

						dbHelper.insertIntoSurveys(database, ssurveyId, ssurveyname, ssurveydesc,
								ssurveystartdate, ssurveyenddate, screatedon, screatedby,
								smodifiedon, ssurveytypeid, ssurveytypename, ssurveytypedesc);

					 }

					 updateProgress("Parsing Data....",45,"Analyzing Surveys",100);
				  }
				  //insert into questions


				  if (jSurveyorData.has(Config.TABLE_QUESTIONS) && !jSurveyorData.isNull(Config
						  .TABLE_QUESTIONS)) {
					 if (jSurveyorData.getJSONArray(Config.TABLE_QUESTIONS).length() > 0) {
						JSONArray ja = jSurveyorData.getJSONArray(Config.TABLE_QUESTIONS);

						int subUp = 100 / ja.length();
						int l=0;

						for (int j = 0; j < ja.length(); j++) {
						   Log.d("Debug", "Question INsertion");
						   //publishProgress(progress,subUpPre+subUp);
						   //progressStatus="Database Created";
						   l=l+subUp;
						   updateProgress("Parsing Data....",60,"Analyzing Questions",l);

						   JSONObject jo = ja.getJSONObject(j);
						   if (jo.has(Config.db.SURVEY_ID) && !jo.isNull(Config.db.SURVEY_ID))
							  qsurveyid = jo.getString(Config.db.SURVEY_ID);
						   else qsurveyid = "";
						   if (jo.has(Config.db.QUESTION_ID) && !jo.isNull(Config.db.QUESTION_ID))
							  qquestionid = jo.getString(Config.db.QUESTION_ID);
						   else qquestionid = "";
//							  if (jo.has(Config.db.QUESTION_TYPE) && !jo.isNull(Config.db.QUESTION_TYPE))
//								 qquestiontype = jo.getString(Config.db.QUESTION_TYPE);
//							  else qquestiontype = "";
						   if (jo.has(Config.db.QUESTION) && !jo.isNull(Config.db.QUESTION))
							  qquestion = jo.getString(Config.db.QUESTION);
						   else qquestion = "";
						   if (jo.has(Config.db.CREATED_ON) && !jo.isNull(Config.db.CREATED_ON))
							  qcreatedon = jo.getString(Config.db.CREATED_ON);
						   else qcreatedon = "";
						   if (jo.has(Config.db.CREATED_BY) && !jo.isNull(Config.db.CREATED_BY))
							  qcreatedby = jo.getString(Config.db.CREATED_BY);
						   else qcreatedby = "";
						   if (jo.has(Config.db.MODIFIED_ON) && !jo.isNull(Config.db.MODIFIED_ON))
							  qmodifiedon = jo.getString(Config.db.MODIFIED_ON);
						   else qmodifiedon = "";

						   dbHelper.insertQuestions(database, qsurveyid, qquestionid,
								   qquestion, qcreatedon, qcreatedby,
								   qmodifiedon);


						}
					 }
				  }

				  if (jSurveyorData.has(Config.QUESTION_OPTION_ARRAY) && !jSurveyorData.isNull(Config
						  .QUESTION_OPTION_ARRAY) && jSurveyorData.getJSONArray(Config
						  .QUESTION_OPTION_ARRAY).length() > 0) {
					 JSONArray array = jSurveyorData.getJSONArray(Config.QUESTION_OPTION_ARRAY);

					 int l=0;
					 int subUp=100/array.length();

					 for (int k = 0; k < array.length(); k++) {
						JSONObject o = array.getJSONObject(k);

						l=l+subUp;
						updateProgress("Parsing Data....",80,"Analyzing Options",l);


						Log.d("Debug", "Option Insertion");
						if (o.has(Config.db.QUESTION_OPTION_ID) && !o.isNull(Config.db
								.QUESTION_OPTION_ID))
						   oquestionoptionid = o.getString(Config.db.QUESTION_OPTION_ID);
						else oquestionoptionid = "";

						if (o.has(Config.db.QUESTION_OPTION) && !o.isNull(Config.db
								.QUESTION_OPTION))
						   oquestionoption = o.getString(Config.db.QUESTION_OPTION);
						else oquestionoption = "";

						if (o.has(Config.db.QUESTION_OPTION_TYPE_ID) && !o.isNull(Config.db
								.QUESTION_OPTION_TYPE_ID))
						   oquestionoptiontypeid = o.getString(Config.db.QUESTION_OPTION_TYPE_ID);
						else oquestionoptiontypeid = "";

						if (o.has(Config.db.QUESTION_OPTION_TYPE) && !o.isNull(Config.db
								.QUESTION_OPTION_TYPE))
						   oquestionoptiontype = o.getString(Config.db.QUESTION_OPTION_TYPE);
						else oquestionoptiontype = "";

						if (o.has(Config.db.QUESTION_ID) && !o.isNull(Config.db
								.QUESTION_ID))
						   oquestionid = o.getString(Config.db.QUESTION_ID);
						else oquestionoptiontype = "";

						if (o.has(Config.db.CREATED_ON) && !o.isNull(Config.db
								.CREATED_ON))
						   ocreatedon = o.getString(Config.db.CREATED_ON);
						else ocreatedon = "";

						if (o.has(Config.db.CREATED_BY) && !o.isNull(Config.db
								.CREATED_BY))
						   ocreatedby = o.getString(Config.db.CREATED_BY);
						else ocreatedby = "";

						if (o.has(Config.db.MODIFIED_ON) && !o.isNull(Config.db
								.MODIFIED_ON))
						   omodifiedon = o.getString(Config.db.MODIFIED_ON);
						else omodifiedon = "";
						dbHelper.insertOptions(database, oquestionid,
								oquestionoptionid, oquestionoption,
								oquestionoptiontypeid, oquestionoptiontype, ocreatedon,
								ocreatedby, omodifiedon);
					 }

					 isDownloadAndInsertOk = true;
					 updateProgress("Parsing Data....",80,"Analyzing Options",100);

				  }
			   }


			} else {
			   if (jsonObject.has(Config.STATUS_MESSAGE) && !jsonObject.isNull(Config.STATUS_MESSAGE)) {
//				  dosomething while status message is null
				  isDownloadAndInsertOk = false;
				  messageError=jsonObject.getString(Config.STATUS_MESSAGE);

			   }
			}

		 } catch (JSONException e) {
			Log.e("Path", e.getMessage().toString());
			DialogCustom dialogCustom=new DialogCustom();
			messageError=e.getMessage();
			dialogCustom.init("Invalid Data", e.getMessage(), "Cancel", "Report", new DialogInterface.OnClickListener() {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
						  //Todo report here
						  ReportToZn reportToZn=new ReportToZn(getApplicationContext());
						  reportToZn.execute(messageError);
					   }
					},
					new DialogInterface.OnClickListener() {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
						  //Todo cancel here
					   }
					}
			);
			dialogCustom.show(getSupportFragmentManager(),"Error");
			e.printStackTrace();
		 } catch (IOException e) {

			Log.e("Path", e.getMessage().toString());
			DialogCustom dialogCustom=new DialogCustom();
			messageError=e.getMessage();
			dialogCustom.init("Error While Connecting!", e.getMessage(), "Cancel", "Report", new
							DialogInterface.OnClickListener() {
							   @Override
							   public void onClick(DialogInterface dialog, int which) {
								  //Todo report here
								  ReportToZn reportToZn=new ReportToZn(getApplicationContext());
								  reportToZn.execute(messageError);
							   }
							},
					new DialogInterface.OnClickListener() {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
						  //Todo cancel here
					   }
					}
			);
			dialogCustom.show(getSupportFragmentManager(),"Error");
			e.printStackTrace();
		 }
		 return 0;
	  }


	  @Override
	  protected void onPostExecute(Integer integer) {
		 super.onPostExecute(integer);

		 updateProgress("Done!", 100, "Done!", 100);

		 if (isDownloadAndInsertOk) {
//			preferences.getBoolean(Config.LOGGED_IN, false);
			preferences.edit().putBoolean(Config.LOGGED_IN,true).apply();
			Intent intent = new Intent(LoginScreen.this, SurveyList.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			dbHelper.close();
		 }else {
			preferences.edit().putBoolean(Config.LOGGED_IN,false).apply();
			Log.e("Path", messageError);
			DialogCustom dialogCustom=new DialogCustom();
//			messageError=e.getMessage();
			dialogCustom.init("Invalid Data", messageError, "Cancel", "Report", new DialogInterface
							.OnClickListener() {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
						  //Todo report here
						  ReportToZn reportToZn=new ReportToZn(getApplicationContext());
						  reportToZn.execute(messageError);
					   }
					},
					new DialogInterface.OnClickListener() {
					   @Override
					   public void onClick(DialogInterface dialog, int which) {
						  //Todo cancel here
					   }
					}
			);
			dialogCustom.show(getSupportFragmentManager(), "Error");
		 }
	  }


	  @Override
	  protected void onProgressUpdate(Integer... values) {
		 super.onProgressUpdate(values);
//		 if (progressBarFinal!=null&&progressBarSub!=null&&textfinal!=null&&textsub!=null)
//		 {
		 progressBarSub.setProgress(progIntSub);
		 progressBarFinal.setProgress(progInt);
		 textfinal.setText(progressStatus);
		 textsub.setText(progressStatussub);
//		 }
	  }

	  private void updateProgress(String status,int progress,String substatus,int progsub){
		 this.progressStatus=status;
		 this.progInt=progress;
		 this.progressStatussub=substatus;
		 this.progIntSub=progsub;
		 publishProgress();
	  }
   }

   public void showLayoutInitialization(){
	  RelativeLayout layoutLogin= (RelativeLayout) findViewById(R.id.rel_login_roottouserpassword);
	  layoutLogin.setVisibility(View.GONE);
	  RelativeLayout layoutInitializaionprocess= (RelativeLayout) findViewById(R.id.rel_initialization_login);
	  layoutInitializaionprocess.setVisibility(View.VISIBLE);
   }
}
