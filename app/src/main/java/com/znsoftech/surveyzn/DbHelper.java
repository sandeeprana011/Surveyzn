package com.znsoftech.surveyzn;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.znsoftech.surveyzn.data_structure.Question;
import com.znsoftech.surveyzn.data_structure.QuestionOptions;
import com.znsoftech.surveyzn.data_structure.SurveyDetailInfo;

import java.util.ArrayList;

/**
 * Created by sandeeprana on 22/09/15.
 */
public class DbHelper extends SQLiteOpenHelper {
   public DbHelper(Context context) {
	  super(context, "/mnt/sdcard/survey.db"
//						+Config.DATABASE_NAME
			  , null, Config.DATABASE_VERSION);
   }

   @Override
   public void onCreate(SQLiteDatabase sqLiteDatabase) {


	  Log.e("Path", "Started creating database");
	  /**
	   * Creating table for surveyor's profile
	   */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_SURVEYOR_PROFILE +
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +
					  Config.db.SURVEYOR_ID + " text," +
					  Config.db.SURVEYOR_CODE + " text," +
					  Config.db.FIRSTNAME + " text," +
					  Config.db.MIDDLE_NAME + " text," +
					  Config.db.LASTNAME + " text," +
					  Config.db.ADDRESS + " text," +
					  Config.db.CITY + " text," +
					  Config.db.STATE + " text," +
					  Config.db.COUNTRY + " text," +
					  Config.db.ZIPCODE + " text," +
					  Config.db.MOBILE + " text," +
					  Config.db.PHONE + " text," +
					  Config.db.ALTERNATE_PHONE + " text," +
					  Config.db.GENDER + " text," +
					  Config.db.DOB + " text," +
					  Config.db.LOGIN + " text," +
					  Config.db.PASSWORD + " text," +
					  Config.db.EMAIL + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text," +
					  Config.db.LATITUDE + " text," +
					  Config.db.LONGITUDE + " text," +
					  Config.db.MARITAL_STATUS + " text" +
					  ")"
	  );


	  /**
	   * Creating table to store consumer's profile
	   */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_CONSUMER_PROFILE +                           //profile of the consumer
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +
					  Config.db.SURVEY_ID + " text," +
					  Config.db.SURVEYOR_ID + " text," +
					  Config.db.FIRSTNAME + " text," +
					  Config.db.MIDDLE_NAME + " text," +
					  Config.db.LASTNAME + " text," +
					  Config.db.ADDRESS + " text," +
					  Config.db.CITY + " text," +
					  Config.db.STATE + " text," +
					  Config.db.COUNTRY + " text," +
					  Config.db.ZIPCODE + " text," +
					  Config.db.MOBILE + " text," +
					  Config.db.PHONE + " text," +
					  Config.db.GENDER + " text," +
					  Config.db.PHOTO + " text," +
					  Config.db.DOB + " text," +
					  Config.db.EMAIL + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text," +
					  Config.db.LATITUDE + " text," +
					  Config.db.LONGITUDE + " text," +
					  Config.db.MARITAL_STATUS + " text" +
					  ")"
	  );


	  /**
	   * creating table for storing surveys
	   */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_SURVEYS +
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +

					  Config.db.SURVEY_ID + " text," +
					  Config.db.SURVEY_NAME + " text," +
					  Config.db.SURVEY_DESC + " text," +
					  Config.db.SURVEY_START_DATE + " text," +
					  Config.db.SURVEY_END_DATE + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text," +
					  Config.db.SURVEY_TYPE_ID + " text," +
					  Config.db.SURVEY_TYPE_NAME + " text," +
					  Config.db.SURVEY_TYPE_DESC + " text," +
					  Config.db.SURVEYING_TIME_TOTAL + " integer)"
	  );


	  /**
	   * TO CREATE THE TABLE OF QUESTIONS
	   */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_QUESTIONS +
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +

					  Config.db.SURVEY_ID + " text," +
					  Config.db.QUESTION_ID + " text," +
					  Config.db.QUESTION + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text)"
	  );
/**
 * TO CREATE THE TABLE OF QUESTIONS_options
 */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_OPTIONS +
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +

					  Config.db.QUESTION_ID + " text," +
					  Config.db.QUESTION_OPTION_ID + " text," +
					  Config.db.QUESTION_OPTION + " text," +
					  Config.db.QUESTION_OPTION_TYPE_ID + " text," +
					  Config.db.QUESTION_OPTION_TYPE + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text)"
	  );

/**
 * TO CREATE THE TABLE OF ANSWERS
 */
	  sqLiteDatabase.execSQL(
			  "create table " + Config.TABLE_ANSWERS +
					  "(" +
					  Config._ID + Config.TYPE_INTEGER_WITH_SPACE + " primary key autoincrement," +
					  Config.db.SURVEY_ID + " text," +
					  Config.db.QUESTION_ID + " text," +
					  Config.db.CONSUMER_ID + " text," + //ACTUALLY IT IS _ID FROM PROFILE TABLE
					  Config.db.ANSWER + " text," +
					  Config.db.ANSWER_TYPE + " text," +
					  Config.db.LATITUDE + " text," +
					  Config.db.LONGITUDE + " text," +
					  Config.db.CREATED_ON + " text," +
					  Config.db.CREATED_BY + " text," +
					  Config.db.MODIFIED_ON + " text)"
	  );
	  Log.e("Path", "Executed");
	  Log.e("Path", sqLiteDatabase.getPath() + "-------" + sqLiteDatabase.getVersion());
   }


   /**
	* insert data into surveyor's Profiles table
	* this is surveyorsProfile
	*
	* @param database
	* @param surveyorId
	* @param surveyorCode
	* @param firstName
	* @param middleName
	* @param lastName
	* @param address
	* @param city
	* @param state
	* @param country
	* @param zipCode
	* @param mobile
	* @param phone
	* @param alternatePhone
	* @param gender
	* @param dOB
	* @param login
	* @param password
	* @param email
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @return
	*/
   public long insertSurveyorProfile(SQLiteDatabase database,
									 String surveyorId,
									 String surveyorCode,
									 String firstName,
									 String middleName,
									 String lastName,
									 String address,
									 String city,
									 String state,
									 String country,
									 String zipCode,
									 String mobile,
									 String phone,
									 String alternatePhone,
									 String gender,
									 String dOB,
									 String login,
									 String password,
									 String email,
									 String createdOn,
									 String createdBy,
									 String modifiedOn,
									 String latitude, //last login location
									 String longitude,//last login location
									 String maritalStatus//currently not in use

   ) {

	  ContentValues values = new ContentValues();

	  values.put(Config.db.SURVEYOR_ID, surveyorId);
	  values.put(Config.db.SURVEYOR_CODE, surveyorCode);
	  values.put(Config.db.FIRSTNAME, firstName);
	  values.put(Config.db.MIDDLE_NAME, middleName);
	  values.put(Config.db.LASTNAME, lastName);
	  values.put(Config.db.ADDRESS, address);
	  values.put(Config.db.CITY, city);
	  values.put(Config.db.STATE, state);
	  values.put(Config.db.COUNTRY, country);
	  values.put(Config.db.ZIPCODE, zipCode);
	  values.put(Config.db.MOBILE, mobile);
	  values.put(Config.db.PHONE, phone);
	  values.put(Config.db.ALTERNATE_PHONE, alternatePhone);
	  values.put(Config.db.GENDER, gender);
	  values.put(Config.db.DOB, dOB);
	  values.put(Config.db.LOGIN, login);
	  values.put(Config.db.PASSWORD, password);
	  values.put(Config.db.EMAIL, email);
	  values.put(Config.db.CREATED_ON, createdOn);
	  values.put(Config.db.CREATED_BY, createdBy);
	  values.put(Config.db.MODIFIED_ON, modifiedOn);
	  values.put(Config.db.LATITUDE, latitude);
	  values.put(Config.db.LONGITUDE, longitude);
	  values.put(Config.db.MARITAL_STATUS, maritalStatus);

	  long rowId_or_Error = database.insert(Config.TABLE_SURVEYOR_PROFILE, null, values);

	  return rowId_or_Error;
   }


   /**
	* Insert data into consumer profiles
	*
	* @param database
	* @param surveyId
	* @param surveyorId
	* @param firstName
	* @param middleName
	* @param lastName
	* @param address
	* @param city
	* @param state
	* @param country
	* @param zipCode
	* @param mobile
	* @param phone
	* @param gender
	* @param photo
	* @param dOB
	* @param email
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @param latitude
	* @param longitude
	* @param maritalStatus
	* @return
	*/
   public long insertConsumerProfiles(SQLiteDatabase database,
									  String surveyId,
									  String surveyorId,
									  String firstName,
									  String middleName,
									  String lastName,
									  String address,
									  String city,
									  String state,
									  String country,
									  String zipCode,
									  String mobile,
									  String phone,
									  String gender,
									  String photo,
									  String dOB,
									  String email,
									  String createdOn,
									  String createdBy,
									  String modifiedOn,
									  String latitude,
									  String longitude,
									  String maritalStatus
   ) {

	  ContentValues values = new ContentValues();

	  values.put(Config.db.SURVEY_ID, surveyId);
	  values.put(Config.db.SURVEYOR_ID, surveyorId);
	  values.put(Config.db.FIRSTNAME, firstName);
	  values.put(Config.db.MIDDLE_NAME, middleName);
	  values.put(Config.db.LASTNAME, lastName);
	  values.put(Config.db.ADDRESS, address);
	  values.put(Config.db.CITY, city);
	  values.put(Config.db.STATE, state);
	  values.put(Config.db.COUNTRY, country);
	  values.put(Config.db.ZIPCODE, zipCode);
	  values.put(Config.db.MOBILE, mobile);
	  values.put(Config.db.PHONE, phone);
//      values.put(Config.db.ALTERNATE_PHONE,alternatePhone);
	  values.put(Config.db.GENDER, gender);
	  values.put(Config.db.PHOTO, photo);
	  values.put(Config.db.DOB, dOB);
	  values.put(Config.db.EMAIL, email);
	  values.put(Config.db.CREATED_ON, createdOn);
	  values.put(Config.db.CREATED_BY, createdBy);
	  values.put(Config.db.MODIFIED_ON, modifiedOn);
	  values.put(Config.db.LATITUDE, latitude);
	  values.put(Config.db.LONGITUDE, longitude);
	  values.put(Config.db.MARITAL_STATUS, maritalStatus);


	  long rowId_or_Error = database.insert(Config.TABLE_CONSUMER_PROFILE, null, values);

	  return rowId_or_Error;
   }


   /**
	* Insert data into surveys table
	*
	* @param sqLiteDatabase
	* @param surveyId
	* @param surveyName
	* @param surveyDesc
	* @param surveyStartDate
	* @param surveyEndDate
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @param surveyTypeId
	* @param surveyTypeName
	* @param surveyTypeDesc
	* @return
	*/
   public long insertIntoSurveys(SQLiteDatabase sqLiteDatabase,
								 String surveyId,
								 String surveyName,
								 String surveyDesc,
								 String surveyStartDate,
								 String surveyEndDate,
								 String createdOn,
								 String createdBy,
								 String modifiedOn,
								 String surveyTypeId,
								 String surveyTypeName,
								 String surveyTypeDesc
   ) {
	  ContentValues contentValues = new ContentValues();
	  contentValues.put(Config.db.SURVEY_ID, surveyId);
	  contentValues.put(Config.db.SURVEY_NAME, surveyName);
	  contentValues.put(Config.db.SURVEY_DESC, surveyDesc);
	  contentValues.put(Config.db.SURVEY_START_DATE, surveyStartDate);
	  contentValues.put(Config.db.SURVEY_END_DATE, surveyEndDate);
	  contentValues.put(Config.db.CREATED_ON, createdOn);
	  contentValues.put(Config.db.CREATED_BY, createdBy);
	  contentValues.put(Config.db.MODIFIED_ON, modifiedOn);
	  contentValues.put(Config.db.SURVEY_TYPE_ID, surveyTypeId);
	  contentValues.put(Config.db.SURVEY_TYPE_NAME, surveyTypeName);
	  contentValues.put(Config.db.SURVEY_TYPE_DESC, surveyTypeDesc);
	  contentValues.put(Config.db.SURVEYING_TIME_TOTAL, 0L); //this parameter will keep in mind
	  // that we have to add some more time to keep the record up that how much our surveyor go
	  // through it to make this happen

	  long statusExecution = sqLiteDatabase.insert(Config.TABLE_SURVEYS, null, contentValues);

	  return statusExecution;
   }


   /**
	* Insert data into Questions table
	*
	* @param database
	* @param surveyId
	* @param questionId
	* @param question
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @return
	*/
   public long insertQuestions(SQLiteDatabase database,
							   String surveyId,
							   String questionId,
							   String question,
							   String createdOn,
							   String createdBy,
							   String modifiedOn
   ) {
	  ContentValues values = new ContentValues();
	  values.put(Config.db.SURVEY_ID, surveyId);
	  values.put(Config.db.QUESTION_ID, questionId);
	  values.put(Config.db.QUESTION, question);
	  values.put(Config.db.CREATED_ON, createdOn);
	  values.put(Config.db.CREATED_BY, createdBy);
	  values.put(Config.db.MODIFIED_ON, modifiedOn);
	  long rowId_or_Error = database.insert(Config.TABLE_QUESTIONS, null, values);
	  return rowId_or_Error;
   }

   /**
	* Insert data into Options table
	*
	* @param database             database instance
	* @param questionId           QuestionId
	* @param questionOptionId
	* @param questionOption
	* @param questionOptionTypeId
	* @param questionOptionType
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @return
	*/

   public long insertOptions(SQLiteDatabase database,
							 String questionId,
							 String questionOptionId,
							 String questionOption,
							 String questionOptionTypeId,
							 String questionOptionType,
							 String createdOn,
							 String createdBy,
							 String modifiedOn
   ) {
	  ContentValues values = new ContentValues();
	  values.put(Config.db.QUESTION_ID, questionId);
	  values.put(Config.db.QUESTION_OPTION_ID, questionOptionId);
	  values.put(Config.db.QUESTION_OPTION, questionOption);
	  values.put(Config.db.QUESTION_OPTION_TYPE, questionOptionType); //Radio,CheckBox,Text
	  values.put(Config.db.QUESTION_OPTION_TYPE_ID, questionOptionTypeId);
	  values.put(Config.db.CREATED_ON, createdOn);
	  values.put(Config.db.CREATED_BY, createdBy);
	  values.put(Config.db.MODIFIED_ON, modifiedOn);


	  long rowId_or_Error = database.insert(Config.TABLE_OPTIONS, null, values);
	  return rowId_or_Error;
   }

   /**
	* Insert into Answers
	*
	* @param database
	* @param surveyId
	* @param questionId
	* @param consumerId
	* @param answer
	* @param answerType
	* @param latitude
	* @param longitude
	* @param createdOn
	* @param createdBy
	* @param modifiedOn
	* @return
	*/

   public long insertAnswers(SQLiteDatabase database,
							 String surveyId,
							 String questionId,
							 String consumerId,
							 String answer,
							 String answerType,
							 String latitude,
							 String longitude,
							 String createdOn,
							 String createdBy,
							 String modifiedOn
   ) {
	  ContentValues values = new ContentValues();
	  values.put(Config.db.SURVEY_ID, surveyId);
	  values.put(Config.db.QUESTION_ID, questionId);
	  values.put(Config.db.CONSUMER_ID, consumerId);
	  values.put(Config.db.ANSWER, answer); //Radio,CheckBox,Text
	  values.put(Config.db.ANSWER_TYPE, answerType);
	  values.put(Config.db.LATITUDE, latitude);
	  values.put(Config.db.LONGITUDE, longitude);
	  values.put(Config.db.CREATED_ON, createdOn);
	  values.put(Config.db.CREATED_BY, createdBy);
	  values.put(Config.db.MODIFIED_ON, modifiedOn);


	  long rowId_or_Error = database.insert(Config.TABLE_ANSWERS, null, values);
	  return rowId_or_Error;
   }

   public ArrayList<SurveyDetailInfo> querySurveysAll(SQLiteDatabase database) {

	  ArrayList<SurveyDetailInfo> arrayListsurveys = new ArrayList<SurveyDetailInfo>();
	  String[] ar = new String[]{Config._ID,
			  Config.db.SURVEY_ID,
			  Config.db.SURVEY_NAME,
			  Config.db.SURVEY_DESC,
			  Config.db.SURVEY_START_DATE,
			  Config.db.SURVEY_END_DATE,
			  Config.db.CREATED_ON,
			  Config.db.CREATED_BY,
			  Config.db.MODIFIED_ON,
			  Config.db.SURVEY_TYPE_ID,
			  Config.db.SURVEY_TYPE_NAME,
			  Config.db.SURVEY_TYPE_DESC
	  };

	  Cursor cursor = database.query(Config.TABLE_SURVEYS, ar, null, null, null, null, null);
	  cursor.moveToFirst();

	  for (int g = 0; g < cursor.getCount(); g++) {
		 SurveyDetailInfo info = new SurveyDetailInfo();
		 info.setSurveyId(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_ID)));
		 info.setSurveyTypeName(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_TYPE_NAME)));
		 info.setSurveyTypeId(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_TYPE_ID)));
		 info.setSurveyTypeDesc(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_TYPE_DESC)));
		 info.setSurveyStartDate(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_START_DATE)));
		 info.setSurveyName(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_NAME)));
		 info.setSurveyEndDate(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_END_DATE)));
		 info.setSurveyDesc(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_DESC)));
		 info.setCreatedBy(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_BY)));
		 info.setCreatedOn(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_ON)));
		 info.setModifiedOn(cursor.getString(cursor.getColumnIndex(Config.db.MODIFIED_ON)));
		 arrayListsurveys.add(info);
		 cursor.moveToNext();
	  }
	  return arrayListsurveys;
   }

   public ArrayList<Question> getQuestions(SQLiteDatabase sqLiteDatabase, String surveyID) {
	  ArrayList<Question> questionArrayList = new ArrayList<>();
	  String[] columns = new String[]{
			  Config.db.SURVEY_ID,
			  Config.db.QUESTION_ID,
			  Config.db.QUESTION,
			  Config.db.CREATED_ON,
			  Config.db.CREATED_BY,
			  Config.db.MODIFIED_ON
	  };
	  Cursor cursor = sqLiteDatabase.query(Config.TABLE_QUESTIONS, columns,
			  Config.db.SURVEY_ID + "=\"" + surveyID + "\"",
			  null,
			  null,
			  null,
			  null);
	  cursor.moveToFirst();


	  for (int g = 0; g < cursor.getCount(); g++) {
		 Question question = new Question();
		 question.setSurveyId(cursor.getString(cursor.getColumnIndex(Config.db.SURVEY_ID)));
		 question.setCreatedBy(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_BY)));
		 question.setCreatedOn(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_ON)));
		 question.setModifiedOn(cursor.getString(cursor.getColumnIndex(Config.db.MODIFIED_ON)));
		 question.setQuestion(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION)));
		 question.setQuestionId(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_ID)));
		 questionArrayList.add(question);
		 cursor.moveToNext();
	  }

	  return questionArrayList;
   }


   public ArrayList<QuestionOptions> getQuestionsOptions(SQLiteDatabase sqLiteDatabase, String
		   questionId) {
	  ArrayList<QuestionOptions> questionOptionArray = new ArrayList<>();

	  String[] columns = new String[]{
			  Config.db.QUESTION_ID,
			  Config.db.QUESTION_OPTION,
			  Config.db.QUESTION_OPTION_ID,
			  Config.db.QUESTION_OPTION_TYPE_ID,
			  Config.db.QUESTION_OPTION_TYPE,
			  Config.db.CREATED_ON,
			  Config.db.CREATED_BY,
			  Config.db.MODIFIED_ON

	  };
	  Cursor cursor = sqLiteDatabase.query(Config.TABLE_OPTIONS, columns, Config.db
					  .QUESTION_ID + "=" + questionId,
			  null,
			  null,
			  null,
			  null);
	  cursor.moveToFirst();


	  for (int g = 0; g < cursor.getCount(); g++) {
		 QuestionOptions questionOption = new QuestionOptions();
		 questionOption.setQuestionId(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_ID)));
		 questionOption.setQuestionOption(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_OPTION)));
		 questionOption.setQuestionOptionId(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_OPTION_ID)));
		 questionOption.setQuestionOptionTypeId(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_OPTION_TYPE_ID)));
		 questionOption.setQuestionOptionType(cursor.getString(cursor.getColumnIndex(Config.db.QUESTION_OPTION_TYPE)));
		 questionOption.setCreatedOn(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_ON)));
		 questionOption.setCreatedBy(cursor.getString(cursor.getColumnIndex(Config.db.CREATED_BY)));
		 questionOption.setModifiedOn(cursor.getString(cursor.getColumnIndex(Config.db.MODIFIED_ON)));
		 questionOptionArray.add(questionOption);
		 cursor.moveToNext();
	  }
	  return questionOptionArray;
   }


   @Override
   public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

   }


}
