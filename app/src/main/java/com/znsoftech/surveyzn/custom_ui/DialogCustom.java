package com.znsoftech.surveyzn.custom_ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by sandeeprana on 20/11/15.
 */
public class DialogCustom extends DialogFragment {
   private  String title,negText,posText;
   DialogInterface.OnClickListener negListener,posListener;
   private String message;

   public void init(String title,String message,String negText,String posText,DialogInterface
		   .OnClickListener
		   neglistener,DialogInterface.OnClickListener posListener){
	  this.title=title;
	  this.negText=negText;
	  this.posText=posText;
	  this.message=message;
	  this.negListener=neglistener;
	  this.posListener=posListener;
   }
   @NonNull
   @Override
   public Dialog onCreateDialog(Bundle savedInstanceState) {
	  AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	  if (negText!=null){
		 return NegDialog(builder).show();
	  }else
	  {
		 return noNegDialog(builder).show();
	  }

   }
   public AlertDialog.Builder noNegDialog(AlertDialog.Builder builder){
	  builder
			  .setTitle(title)
			  .setMessage(message)
			  .setPositiveButton(posText, posListener);
	  return builder;
   }

   public AlertDialog.Builder NegDialog(AlertDialog.Builder builder){
	  builder
			  .setTitle(title)
			  .setMessage(message)
			  .setPositiveButton(posText,posListener)
			  .setNegativeButton(negText,negListener);
	  return builder;
   }
}
