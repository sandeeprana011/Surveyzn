package com.znsoftech.surveyzn.network;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.znsoftech.surveyzn.Config;

import java.io.IOException;

/**
 * Created by sandeeprana on 20/11/15.
 */
public class ReportToZn extends AsyncTask<String,Integer,String> {

   private Context context;

   public ReportToZn(Context context){
	  this.context=context;
   }
   private String submit;

   @Override
   protected void onPreExecute() {
	  super.onPreExecute();
   }

   @Override
   protected String doInBackground(String... params) {
	  try {
		 submit=DownloadContentAndDecode.downloadContentUsingPostMethod(Config.REPORT_URL,
				 "error=" + params[0]);
	  } catch (IOException e) {
		 e.printStackTrace();
	  }
	  return submit;
   }

   @Override
   protected void onPostExecute(String s) {
	  super.onPostExecute(s);
	  if (submit.equals("success")){
		 Toast.makeText(context,"Report submitted!",Toast.LENGTH_LONG).show();
	  }else {
		 Toast.makeText(context,"Report submition failed!",Toast.LENGTH_LONG).show();
	  }
   }
}
