package com.znsoftech.surveyzn.data_structure;

import java.io.Serializable;

/**
 * Created by sandeeprana on 30/09/15.
 * This class contains the detailed surveyDesc of the Survey use it carefully takes more memory than Survey Brief
 */
public class SurveyDetailInfo implements Serializable {
   private String SurveyId;
   private String surveyName;
   private String surveyDesc;
   private String surveyStartDate;
   private String surveyEndDate;
   private String createdOn;
   private String createdBy;
   private String modifiedOn;
   private String surveyTypeId;
   private String surveyTypeName;
   private String surveyTypeDesc;

   public String getSurveyName() {
	  return surveyName;
   }

   public void setSurveyName(String surveyName) {
	  this.surveyName = surveyName;
   }

   public String getSurveyDesc() {
	  return surveyDesc;
   }

   public void setSurveyDesc(String surveyDesc) {
	  this.surveyDesc = surveyDesc;
   }

   public String getSurveyStartDate() {
	  return surveyStartDate;
   }

   public void setSurveyStartDate(String surveyStartDate) {
	  this.surveyStartDate = surveyStartDate;
   }

   public String getSurveyEndDate() {
	  return surveyEndDate;
   }

   public void setSurveyEndDate(String surveyEndDate) {
	  this.surveyEndDate = surveyEndDate;
   }

   public String getCreatedOn() {
	  return createdOn;
   }

   public void setCreatedOn(String createdOn) {
	  this.createdOn = createdOn;
   }

   public String getCreatedBy() {
	  return createdBy;
   }

   public void setCreatedBy(String createdBy) {
	  this.createdBy = createdBy;
   }

   public String getModifiedOn() {
	  return modifiedOn;
   }

   public void setModifiedOn(String modifiedOn) {
	  this.modifiedOn = modifiedOn;
   }

   public String getSurveyTypeId() {
	  return surveyTypeId;
   }

   public void setSurveyTypeId(String surveyTypeId) {
	  this.surveyTypeId = surveyTypeId;
   }

   public String getSurveyTypeName() {
	  return surveyTypeName;
   }

   public void setSurveyTypeName(String surveyTypeName) {
	  this.surveyTypeName = surveyTypeName;
   }

   public String getSurveyTypeDesc() {
	  return surveyTypeDesc;
   }

   public void setSurveyTypeDesc(String surveyTypeDesc) {
	  this.surveyTypeDesc = surveyTypeDesc;
   }

   public String getSurveyId() {
	  return SurveyId;
   }

   public void setSurveyId(String surveyId) {
	  SurveyId = surveyId;
   }
}
