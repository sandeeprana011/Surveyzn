package com.znsoftech.surveyzn.data_structure;

import java.util.ArrayList;

/**
 * Created by sandeeprana on 16/10/15.
 */
public class Question {
   private String surveyId;
   private String questionId;
   private String question;
   private String createdOn;
   private String createdBy;
   private String modifiedOn;
   private ArrayList<QuestionOptions> optionsArrayList;

   /**
	* In this case you have to explicitly define every single parameter
	*/
   public Question() {

   }

   public Question(String surveyId,
				   String questionId,
				   String question,
				   String createdOn,
				   String createdBy,
				   String modifiedOn) {
	  this.surveyId = surveyId;
	  this.questionId = questionId;
	  this.question = question;
	  this.createdOn = createdOn;
	  this.createdBy = createdBy;
	  this.modifiedOn = modifiedOn;
   }

   public String getSurveyId() {
	  return surveyId;
   }

   public void setSurveyId(String surveyId) {
	  this.surveyId = surveyId;
   }

   public String getQuestionId() {
	  return questionId;
   }

   public void setQuestionId(String questionId) {
	  this.questionId = questionId;
   }

   public String getQuestion() {
	  return question;
   }

   public void setQuestion(String question) {
	  this.question = question;
   }

   public String getCreatedOn() {
	  return createdOn;
   }

   public void setCreatedOn(String createdOn) {
	  this.createdOn = createdOn;
   }

   public String getCreatedBy() {
	  return createdBy;
   }

   public void setCreatedBy(String createdBy) {
	  this.createdBy = createdBy;
   }

   public String getModifiedOn() {
	  return modifiedOn;
   }

   public void setModifiedOn(String modifiedOn) {
	  this.modifiedOn = modifiedOn;
   }

   public ArrayList<QuestionOptions> getOptionsArrayList() {
	  return optionsArrayList;
   }

   public void setOptionsArrayList(ArrayList<QuestionOptions> optionsArrayList) {
	  this.optionsArrayList = optionsArrayList;
   }
}
