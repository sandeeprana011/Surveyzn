package com.znsoftech.surveyzn.data_structure;

/**
 * Created by sandeeprana on 30/09/15.
 * This class contains few details which helps in less memory consumption
 */
public class SurveyBrief {
   private String surveyName;
   private String surveyDesc;
   private String surveyStartDate;
   private String surveyEndDate;
   private String surveyId;

   /**
	* @return SurveyName
	*/
   public String getSurveyName() {
	  return surveyName;
   }

   public void setSurveyName(String surveyName) {
	  this.surveyName = surveyName;
   }

   public String getSurveyDesc() {
	  return surveyDesc;
   }

   public void setSurveyDesc(String surveyDesc) {
	  this.surveyDesc = surveyDesc;
   }

   public String getSurveyStartDate() {
	  return surveyStartDate;
   }

   public void setSurveyStartDate(String surveyStartDate) {
	  this.surveyStartDate = surveyStartDate;
   }

   public String getSurveyEndDate() {
	  return surveyEndDate;
   }

   public void setSurveyEndDate(String surveyEndDate) {
	  this.surveyEndDate = surveyEndDate;
   }

   public String getSurveyId() {
	  return surveyId;
   }

   public void setSurveyId(String surveyId) {
	  this.surveyId = surveyId;
   }
}
