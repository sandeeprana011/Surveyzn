package com.znsoftech.surveyzn.data_structure;

import android.os.Environment;

/**
 * Created by sandeeprana on 16/10/15.
 */
public class FileStor {/* Checks if external storage is available for read and write */

   public static String DIR_NAME = "SurveyZN";

   public static boolean isExternalStorageWritable() {
	  String state = Environment.getExternalStorageState();
	  if (Environment.MEDIA_MOUNTED.equals(state)) {
		 return true;
	  }
	  return false;
   }

   /* Checks if external storage is available to at least read */
   public boolean isExternalStorageReadable() {
	  String state = Environment.getExternalStorageState();
	  if (Environment.MEDIA_MOUNTED.equals(state) ||
			  Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		 return true;
	  }
	  return false;
   }
}
