package com.znsoftech.surveyzn.data_structure;

/**
 * Created by sandeeprana on 16/10/15.
 */
public class QuestionOptions {
   private String questionId;
   private String questionOptionId;
   private String questionOptionText;
   private String questionOptionTypeId;
   private String questionOptionType;
   private String createdOn;
   private String createdBy;
   private String modifiedOn;

   public QuestionOptions() {

   }

   public QuestionOptions(String questionId,
						  String questionOptionId,
						  String questionOptionText,
						  String questionOptionTypeId,
						  String questionOptionType,
						  String createdOn,
						  String createdBy,
						  String modifiedOn
   ) {
	  this.setQuestionId(questionId);
	  this.setQuestionOptionId(questionOptionId);
	  this.setQuestionOption(questionOptionText);
	  this.setQuestionOptionTypeId(questionOptionTypeId);
	  this.setQuestionOptionType(questionOptionType);
	  this.setCreatedOn(createdOn);
	  this.setCreatedBy(createdBy);
	  this.setModifiedOn(modifiedOn);

   }

   public String getQuestionId() {
	  return questionId;
   }

   public void setQuestionId(String questionId) {
	  this.questionId = questionId;
   }

   public String getQuestionOptionId() {
	  return questionOptionId;
   }

   public void setQuestionOptionId(String questionOptionId) {
	  this.questionOptionId = questionOptionId;
   }

   public String getQuestionOptionText() {
	  return questionOptionText;
   }

   public String getQuestionOptionTypeId() {
	  return questionOptionTypeId;
   }

   public void setQuestionOptionTypeId(String questionOptionTypeId) {
	  this.questionOptionTypeId = questionOptionTypeId;
   }

   public String getQuestionOptionType() {
	  return questionOptionType;
   }

   public void setQuestionOptionType(String questionOptionType) {
	  this.questionOptionType = questionOptionType;
   }

   public String getCreatedOn() {
	  return createdOn;
   }

   public void setCreatedOn(String createdOn) {
	  this.createdOn = createdOn;
   }

   public String getCreatedBy() {
	  return createdBy;
   }

   public void setCreatedBy(String createdBy) {
	  this.createdBy = createdBy;
   }

   public String getModifiedOn() {
	  return modifiedOn;
   }

   public void setModifiedOn(String modifiedOn) {
	  this.modifiedOn = modifiedOn;
   }

   public void setQuestionOption(String questionOptionText) {
	  this.questionOptionText = questionOptionText;
   }
}
