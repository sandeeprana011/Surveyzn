package com.znsoftech.surveyzn;

import android.provider.BaseColumns;

/**
 * Created by sandeeprana on 22/09/15.
 */
public class Config implements BaseColumns {
   public static final String TABLE_SURVEYOR_PROFILE = "profile";
   public static final String TABLE_CONSUMER_PROFILE = "consumer_profile";
   public static final String TABLE_SURVEYS = "surveys";
   public static final String TABLE_QUESTIONS = "questions";
   /**
	* TYPE TEXT WITH SPACE AT STARTING LIKE " text"
	*/
   public static final String TYPE_TEXT_WITH_SPACE = " text";
   /**
	* TYPE INTEGER WITH SPACE AT STARTING LIKE " integer"
	*/
   public static final String TYPE_INTEGER_WITH_SPACE = " integer";
   /**
	* COMMA SEPARATOTR
	*/
   public static final String COM_SEP = ",";
   public static final String DATABASE_NAME = "survey.db";
   public static final int DATABASE_VERSION = 1;
   public static final String MARRIED = "Married";
   public static final String UNMARRIED = "Unmarried";
   public static final String TABLE_OPTIONS = "options";
   public static final String STATUS = "status";
   public static final String STATUS_MESSAGE = "status_message";
   public static final String SURVEYOR_DATA = "surveyor_data";
   public static final String QUESTION_OPTION_ARRAY = "questionsoptions";
   public static final String TABLE_ANSWERS = "answers";
   public static final int NO_QUESTION_OPTION = 0;
   public static final int NO_MORE_QUESTION = 1;
   public static final int SUCCESS = 2;
   public static final String URL_PROFILE ="http://survey.znsoftech.com/survey/index" +
		   ".php?r=webservice/getsurvey" ;
   public static final String REPORT_URL = "";
   public static final String NOT_SPECIFIED = "Not_Specified";
   public static final String SETTINGS = "settings";
   public static final String LOGGED_IN = "loggedin";
   public static String LATITUDE_CURRENTUSER = null;
   public static String LONGITUDE_CURRENTUSER = null;
   public static String SURVEYOR_ID_LOGGED_IN;

   public static String OPTION_TYPE_RADIO = "RadioButton";
   public static String OPTION_TYPE_CHECKBOX = "CheckBox";
   public static String OPTION_TYPE_EDITTEXT = "EditText";


   /**
	* To prevent someone to instantiate this class accidentally.
	*/
   public Config() {
   }

   public class db {
	  public static final String SURVEYOR_ID = "SurveyorId";
	  public static final String SURVEYOR_CODE = "SurveyorCode";
	  public static final String FIRSTNAME = "FirstName";
	  public static final String MIDDLE_NAME = "MiddleName";
	  public static final String LASTNAME = "LastName";
	  public static final String ADDRESS = "Address";
	  public static final String CITY = "City";
	  public static final String STATE = "State";
	  public static final String COUNTRY = "Country";
	  public static final String ZIPCODE = "ZipCode";
	  public static final String MOBILE = "Mobile";
	  public static final String PHONE = "Phone";
	  public static final String ALTERNATE_PHONE = "AlternatePhone";
	  public static final String GENDER = "Gender";
	  public static final String DOB = "DOB";
	  public static final String LOGIN = "Login";
	  public static final String PASSWORD = "PASSWORD";
	  public static final String EMAIL = "Email";
	  public static final String CREATED_ON = "CreatedOn";
	  public static final String CREATED_BY = "CreatedBy";

	  public static final String LATITUDE = "Latitude";
	  public static final String LONGITUDE = "Longitude";


	  public static final String CONSUMER_ID = "ConsumerId";

	  public static final String MODIFIED_ON = "ModifiedOn";
	  public static final String MARITAL_STATUS = "MaritalStatus";
	  public static final String QUESTION_ID = "QuestionID";
	  public static final String ANSWER_ID = "AnswerId";
	  public static final String QUESTION = "Question";
	  public static final String QUESTION_OPTION = "QuestionOption";
	  public static final String QUESTION_OPTION_ID = "QuestionOptionID";
	  public static final String QUESTION_OPTION_TYPE_ID = "QuestionOptionTypeID";
	  public static final String QUESTION_OPTION_TYPE = "QuestionOptionType";


	  public static final String SURVEY_ID = "SurveyId";
	  public static final String SURVEY_NAME = "SurveyName";
	  public static final String SURVEY_DESC = "SurveyDesc";
	  public static final String SURVEY_START_DATE = "SurveyStartDate";
	  public static final String SURVEY_END_DATE = "SurveyEndDate";

	  public static final String SURVEY_TYPE_ID = "SurveyTypeId";
	  public static final String SURVEY_TYPE_NAME = "SurveyTypeName";
	  public static final String SURVEY_TYPE_DESC = "SurveyTypeDesc";
	  public static final String PHOTO = "Photo";
	  public static final String ANSWER = "Answer";
	  public static final String ANSWER_TYPE = "AnswerType";
	  public static final String SURVEYING_TIME_TOTAL = "SurveyingTime";
   }
   public class param{
	  public static final String USERNAME="username";
	  public static final String PASSWORD="password";
   }
}