package com.znsoftech.surveyzn;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

/**
 * Created by sandeeprana on 23/10/2015 AD.
 */
public class DialogDatePick extends android.app.DatePickerDialog implements DatePickerDialog.OnDateSetListener {
//   private String curDate;

   public DialogDatePick(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
	  super(context, callBack, year, monthOfYear, dayOfMonth);
   }

   @Override
   public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//	  setCurDate(String.valueOf(year)+"/"+String.valueOf(month)+"/"+String.valueOf(day));
   }
//
//   public String getCurDate() {
//	  return curDate;
//   }
//
//   public void setCurDate(String curDate) {
//	  this.curDate = curDate;
//   }
}
