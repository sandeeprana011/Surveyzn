package com.znsoftech.surveyzn;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sandeeprana on 13/10/15.
 */
public class ConnectionHelper {
   public boolean isConnectedToInternet(Context context) {
	  ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	  NetworkInfo networkInfo = manager.getActiveNetworkInfo();
	  if (networkInfo != null && networkInfo.isConnected()) {
		 return true;
	  } else
		 return false;
   }

}
