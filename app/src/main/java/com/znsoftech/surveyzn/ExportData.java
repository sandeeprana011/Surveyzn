package com.znsoftech.surveyzn;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sandeeprana on 18/11/15.
 */
public class ExportData {

   /**
	* This function iterates through a whole table
	* and collect all the objects and puts them into a json array
	* @param database Sqlitedatabase object
	* @param tableName Tablename asa string
	* @return returns an JSon array containing all the data.
	*/
   public JSONArray jsonArrayReturnTableData(SQLiteDatabase database,String tableName,String[] selection){
	  String query="SELECT * FROM "+tableName;
	  Cursor cursor=database.rawQuery(query,selection);
	  JSONArray array=new JSONArray();
	  cursor.moveToFirst();
	  while (!cursor.isAfterLast()){
		 int totalColumn=cursor.getColumnCount();
		 JSONObject rowJson=new JSONObject();
		 for (int i=0;i<totalColumn;i++){
			if (cursor.getColumnName(i)!=null){
			   try{
				  if (cursor.getString(i)!=null){
					 rowJson.put(cursor.getColumnName(i),cursor.getString(i));
				  } else
				  {
					 rowJson.put( cursor.getColumnName(i) ,  "" );
				  }
			   } catch (JSONException e) {
				  e.printStackTrace();
			   }
			}
		 }
		 array.put(rowJson);
		 cursor.moveToNext();
	  }
	  return array;
   }

}
