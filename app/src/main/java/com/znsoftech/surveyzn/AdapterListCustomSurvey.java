package com.znsoftech.surveyzn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.znsoftech.surveyzn.data_structure.SurveyDetailInfo;

import java.util.ArrayList;

/**
 * Created by sandeeprana on 28/09/15.
 */
public class AdapterListCustomSurvey extends BaseAdapter {
   private final Context contextThis;
   LayoutInflater layoutInflater;

   ArrayList<SurveyDetailInfo> surveyDetailInfoArrayListThis;

   public AdapterListCustomSurvey(ArrayList<SurveyDetailInfo> SurveyDetailArrayList, Context
		   context) {
	  this.surveyDetailInfoArrayListThis = SurveyDetailArrayList;
	  this.contextThis = context;

   }

   @Override
   public int getCount() {
	  return surveyDetailInfoArrayListThis.size();
   }

   /**
	* @param i position of the item you wants
	* @return SurveyBriefInfo Object
	*/

   @Override
   public Object getItem(int i) {
	  return surveyDetailInfoArrayListThis.get(i);
   }

   /**
	* here this will return the number of the current element
	*
	* @param i
	* @return
	*/
   @Override
   public long getItemId(int i) {
	  return i;
   }

   @Override
   public View getView(final int position, View view, ViewGroup viewGroup) {
	  layoutInflater = LayoutInflater.from(contextThis);

	  View listItemView = layoutInflater.inflate(R.layout.list_item_surveylist, null);

	  TextView textView_title = (TextView) listItemView.findViewById(R.id.survey_listitem_title);
	  TextView textView_description = (TextView) listItemView.findViewById(R.id.survey_listitem_dascription);
	  TextView textView_startedOn = (TextView) listItemView.findViewById(R.id.tlabel_startdate);


	  final SurveyDetailInfo surveyDetailInfo = surveyDetailInfoArrayListThis.get(position);

	  textView_title.setText(surveyDetailInfo.getSurveyName());
	  textView_description.setText(surveyDetailInfo.getSurveyDesc());
	  textView_startedOn.setText(surveyDetailInfo.getSurveyStartDate());


	  return listItemView;
   }


}
